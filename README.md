# React Relay GraphQL Node Express Mongo App
Simple React app developed using Relay, GraphQL Node Express and MongoDB
### Install Dependencies
```sh
npm install
```
### Run development
```sh
npm run start
```